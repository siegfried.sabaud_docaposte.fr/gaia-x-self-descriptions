# Roles overview

This file gives an overview of roles in this Self-Description (SD) group, and assigns collaborators to these.

## Roles

__SD Supplier__:
Own/manages any asset (node/participant/service/...) to be described, and can provide information about these in any form.

__SD Developer__:
Implements SDs on a technical level, namely RDF/turtle, JSON-LD, or SHACL shapes.

__Adviser__:
Brings valuable input to this group, in terms of input/requirements or other connection to other workstreams.

## Roles assignment

| Person | SD Supplier | SD Developer | Adviser | Usage(UI/Trust/Catalog/...)
|:-|:-:|:-:|:-:|:-:|
| Johannes Lipp |  | X |  |  |
| Christoph Lange |  | X |  |  |
| Dennis Kubitza |  | X |  |  |
| Sergiu Stejar |  | X |  |  |
| Sebastian Bader |  | X |  |  |
| Jens Plogsties | X |  |  |  |
| Harald Wagener | X | X |  |  |
| Tobias Graf |  | X | X | X |
| Pierre Gronlier | X | X |  |  |
| Berthold Maier |  | X | X |  |
| Jonas Rebstadt |  |  | X |  |
| Mark Kühner |  |  | X |  |
| Joerg Heese | X | X |  | X |
| Antonin Anchisi | X |  | X |  |
| Thomas Feld | X |  |  | X |
| Günter Eggers | X |  | X |  |
| Diamantis Ververis |  |  | X |  |
| Markus Leberecht |  |  | X |  |
| Cornelius Hald | X |  | X |  |
| Martin Voigt | X | X |  |  |
| Olav Strawe |  |  | X | X |
| Valeri Parshin |  |  | X |  |
| Jörg Langkau |  | X |  | (X) |
| Ronny Reinhardt | X |  | X |  |
| Jean Chorin |  |  |  | X |
| Alexander Naumenko | X |  |  |  |
| Gebhard Marent |  |  | X |  |
| Detlef Hühnlein | X |  | X | X |
| Matthias Binzer | X | X | X |  |
| Olivier Tirat | X |  | X |  |
| Ulrich Walter |  |  | X |  |
| Kurt Garloff | X | X |  |  |
| Josep Blanch | X |  |  |  |
