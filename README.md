# GAIA-X Self Descriptions

This repository contains documentation, implementation, and templates on GAIA-X Self Descriptions (SDs).
For an overview, please inspect the `documentation` directory.
We collect all inputs and requirements in any form in the `input+requirements` directory.
In `implementation`, we present and develop all SD-related vocabularies, templates, examples, shapes/rules, and queries.


## How to contribute

Depending on your role (cf. [role overview](/documentation/roles-overview.md), you can contribute in different ways.
- SD suppliers can supply `Provider` SDs as follows:
  - Based on the [Provider Template](/implementation/instances/ProviderExample.ttl), create a copy for you, and adapt/remove/add properties as needed.
- SD developers will mainly work in the [implementation](/implementation) directory.
- Advisers will mainly contribute through the weekly calls as well as through advises on issues, commits, and merge requests.