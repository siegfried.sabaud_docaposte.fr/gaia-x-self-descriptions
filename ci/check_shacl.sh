#!/bin/sh

set -e

IFS="
"

for f in `find ../implementation -name '*.ttl'`; do
    echo "$f"
    pyshacl -m -a -f human -i rdfs "$f"
done
