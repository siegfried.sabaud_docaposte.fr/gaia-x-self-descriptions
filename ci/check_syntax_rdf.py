#!/usr/bin/env python3

import rdflib
import os
from functools import partial
import json
from colorama import Fore, Back, Style

def load_file(filepath, format):
    g = rdflib.Graph()
    g.load(filepath, format=format)
    # print('found {} triples'.format(len(g)))
    # for subject, predicate, obj in g:
    #     print(subject, predicate, obj)


EXTS = {'.ttl': partial(load_file, format='turtle'),
    '.jsonld': partial(load_file, format='json-ld')}

if __name__ == '__main__':
    err_count = 0
    for dirname, _, filenames in os.walk('../implementation'):
        for filename in filenames:
            for ext, loader in EXTS.items():
                if not filename.endswith(ext):
                    continue
                filepath = os.path.join(dirname, filename)
                print(filepath)
                try:
                    loader(filepath)
                except rdflib.plugins.parsers.notation3.BadSyntax as ex:
                    print(Fore.RED + ex.message)
                    print(Style.RESET_ALL)
                    err_count += 1
                except json.decoder.JSONDecodeError as ex:
                    print(Fore.RED + str(ex))
                    print(Style.RESET_ALL)
                    err_count += 1
    if err_count != 0:
        print(Style.BRIGHT + Fore.RED + 'found {} error(s)'.format(err_count))
    os.sys.exit(err_count)
