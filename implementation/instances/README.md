# Examples for GAIA-X

 In this directory and its sub-directories, we organize and develop Self-Descriptions for Providers, Services, and Nodes.
 The current work plan finalizes these three targets in exactly that order.

Please do not enter secret details (passwords or crucial personal information) as all development details might be public.

 ## Current process

| Target | Requirements collected | Template Draft created | Template filled w/ real-world data | Revised template created |
|:-:|:-:|:-:|:-:|:-:|
| Provider | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |
| Service | :heavy_check_mark: | :heavy_check_mark: | :arrows_counterclockwise: |   |
| Node |   |   |   |   |

## Deployment for the MVG

To give dedicated Feedback for all Creators of Provider and Service - Self Descriptions the content within the `provider` and `service` directory is available via github [Pages](https://gaia-x.gitlab.io/gaia-x-core/gaia-x-self-description/files.txt). 
For this every `*.ttl` and `*.jsonld` File within those Directories on master is copied over to Pages from where it is consumed by our [MVG Portal](https://gitlab.com/gaia-x/gaia-x-community/mvg-demonstrator/gaia-x-portal) and inserted in the MVG Database.

> **PLEASE DON'T INSERT ANY CONFIDENTIAL DATA WITHIN YOUR PROVIDER OR SERVICE DESCRIPTION!**
>  
> EVEN IF WE DON'T SHOW THOSE PROPERTIES WITHIN THE PORTAL 
> EVERYTHING IS PUBLIC AVAILABLE!!

When you are describing any possibly Confidential Details like Deployment Details or Contacts fill in some Fake Data to show your Intention only.
